---
layout: blog
title: Welcome to launchpad
date: 2019-12-18T03:04:44.183Z
description: Welcome to launchpad! In this post I discuss why we created launchpad, the decision on the stack we used and just a little bit about how we created our blogging platform in just a couple of hours from concept to launch.
author:
  - Rick Calder
---



Welcome to launchpad, a blog from the creators of RocketParty a group management platform that we hope to be launching soon. During a recent meeting we were discussing how to gain traction and get RocketParty noticed. Sadly the internet is not a "build it and they will come" platform, you can have the best software or website out there and if no one knows it exists you'll fail. My colleague, Nick, suggested creating a technical blog, somewhere that we could write about our journey creating RocketParty among other things. We could talk about stack choices, challenges along the way, etc. 

We agreed this was an idea worth pursuing so then the discussion turned to which blogging platform to use. There's the obvious choice, the choice that already powers far too much of the internet in my opinion, WordPress. There were some pros to this idea, WordPress offers a pretty fast zero to published experience. You can grab a free theme, a couple of plugins, upload to your server and boom you're blogging. WordPress also has some cons, it's... well WordPress. It's pretty heavyweight, its regularly a target for malicious attacks, it isn't terribly performant and honestly after looking for a bit there aren't a ton of good attractive themes. 

So we dug a little deeper, and honestly, there isn't really a lot out there. There is Ghost, which is expensive if you pay for them to host it and kind of limited. I suppose we could have used something like SquareSpace but again an added monthly cost for a brand new startup that wants to limit expenditures for now. There are dozens of small blogging packages on Github but at the end of the day we're developers and liked the idea of building something that was exactly what we wanted it to be.

Enter [Sapper](https://sapper.svelte.dev/). RocketParty's front-end is being built in [Svelte](https://svelte.dev) anyway and Sapper is to Svelte what Next is to React. If you aren't familiar with Svelte I strongly suggest you check it out. At a high level it's very much like React and Vue... without being like either of those at all! Svelte is a framework without actually being a framework, it is really a compiler. If you're interested in that confusing explanation I urge you to check out this talk by Rich Harris the creator of Svelte and Sapper (and Rollup the webpack alternative). [Rethinking Reactivity](https://www.youtube.com/watch?v=AdNJ3fydeao)

If you are familiar with Sapper and Svelte then you'll recognize the layout of this blog. We changed very little about the design of the sample application that you get when you install Sapper. Why change what is already good? We didn't want the blog to take development time away from RocketParty itself. We wanted it to be fast, clean and legible and everything about the look of the sample app was already that. So we invested a couple of hours just setting it up to do what we needed. Which was a pretty short list.

- Parse Markdown files for the actual blog posts with Front Matter for the metadata.
- Have inline code syntax highlighting

The first list item was very straight forward. The Sapper sample application comes with a blog route that is populated from a file that just returns a json object of posts. Installing the fm and marked packages from NPM and slightly modifying the functions that the blog already had to read the markdown files instead of the json object took all of 15 minutes. Yay!

I won't drop those code blocks here, they're a touch long, but you can see them in the repo for this site at [GitLab](https://gitlab.com/rocketparty/launchpad) the files in question are in the src folder.

The second item in the list was just as easy, but took me a lot longer, probably two hours. I am chalking that up to being tired when I was trying to do it and seriously overcomplicating the problem. I used [highlight.js](https://highlightjs.org/) to do the highlighting but I wasn't wrapping my head around how and when the actual HTML was being created. So I kept ending up with the highlighting flashing for a second then reverting to unstyled code blocks.

Like most other problems I face, I walked away. In this case I went to bed, this is a side project and I was working on it at 2am. The next morning I sat down with my coffee and the lightbulb went on. I spent 2 hours the night before trying to look at how the Svelte and Sapper sites did their highlighting. Reading documentation, trying different iterations of code... all to end up with the code below in about 2 minutes the following morning.

<pre>
  <code class="javascript">
    onMount(async  ()  =>  {
      document.querySelectorAll('pre code').forEach((block)  =>  {
        hljs.highlightBlock(block);
      });
    });
  </code>
</pre>

>Pro Tip - If you're tired, go to bed. Also don't overcomplicate things.

There are still a couple things on the wishlist, we'll likely add a search with Solr, we still need to add the obligatory SEO stuff because you know, the internet still isn't a "build it and they will come" platform. But in a couple of hours we have a functional, fast and (we think) attractive blogging platform.

It's not the kind of thing I think I'd give a client, the posts are written in markdown not some overweight wysiwyg. There aren't a ton of plugins to do really hard things like add Google analytics code (kidding lol). And publishing a post means pushing to the git repo (it uses Netlify's great auto deploys). But it works for us and we hope you like it!

Some great upsides to this choice.

- Sapper has an export to static function, so it's fast.
- Static HTML means we can host it on [Netlify](https://netlify.com) so it's cheap.
- No backend or database means it's secure.
- Sapper also does SSR out of the box as well as being setup to be a progressive web app.
- Sapper and Svelte are just javascript, HTML and CSS, there are only a couple of instances of special syntax I have to remember. Which for a developer my age is a God send!