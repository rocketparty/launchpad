# launchpad

RocketParty's blogging platform.

Built using [sapper](https://sapper.svelte.dev), parses markdown files for posts and includes syntax highlighting for code blocks inside the markdown files.

##Usage

```
npm install

# Run local development server on localhost:3000
npm run dev
```

